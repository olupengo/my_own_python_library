import cdsapi
import dateProcess
import itertools


def download_cdsapi(date, time):
    """利用 CDSAPI 下载气候数据

    :param time: 日期
    :param date: 时间

    """
    c = cdsapi.Client()

    fields = date.split('-')  # 拆分日期的年、月、日
    year = fields[0]
    month = fields[1]
    day = fields[2]

    print('-' * 17 + ' Download data at %s-%s-%s %s by CDSAPI ' % (year, month, day, time) + '-' * 17)

    c.retrieve(
        'reanalysis-era5-pressure-levels',
        {
            'product_type': 'reanalysis',
            'variable': [
                'temperature', 'u_component_of_wind', 'v_component_of_wind', 'vertical_velocity',
            ],
            'pressure_level': [
                '1', '2', '3',
                '5', '7', '10',
                '20', '30', '50',
                '70', '100', '125',
                '150', '175', '200',
                '225', '250', '300',
                '350', '400', '450',
                '500', '550', '600',
                '650', '700', '750',
                '775', '800', '825',
                '850', '875', '900',
                '925', '950', '975',
                '1000',
            ],
            'year': year,
            'month': month,
            'day': day,
            'time': time,
            'area': [90, -180, 0, 180],  # North, West, South, East. Default: global
            'format': 'netcdf',  # Supported format: grib and netcdf. Default: grib
            'grid': [3.0, 3.0],  # Latitude/longitude grid. Default: 0.25 deg x 0.25 deg
        },
        './climateInfoNorthernHemisphere/climate_UVOmegaTemp_%s%s%s%s.nc' % (year, month, day, time[:2]))
    print('-' * 4 + ' Datafile climate_UVOmegaTemp_%s%s%s%s.nc' % (
        year, month, day, time) + ' is downloaded successfully ' + '-' * 4 + '\n')


if __name__ == "__main__":
    start_date = "2018-04-26"
    end_date = "2018-10-31"

    # 获取 start_date 与 end_date 之间所有的日期列表
    date_list = dateProcess.create_assist_date(start_date, end_date)
    # print(len(date_list))

    time_list = ["00:00", "06:00", "12:00", "18:00", ]

    lon_list = range(-180, 180 + 1, 3)
    lat_list = range(-90, 90 + 1, 3)

    # 下载文件
    for info in itertools.product(date_list, time_list):
        download_cdsapi(info[0], info[1])
