import math
from datetime import datetime, timedelta


def create_assist_date(datestart: str = None, dateend: str = None):
    """获取指定日期间隔内的日期列表

    Args:
        datestart: 开始日期
        dateend: 结束日期
    Returns:
        date_list: 日期列表 ['2020-01-25', '2020-01-26', '2020-01-27', '2020-01-28',...]

    函数来源：https://blog.csdn.net/qq_38923792/article/details/104184174
    """
    if datestart is None:
        datestart = '2020-01-01'
    if dateend is None:
        dateend = datetime.now().strftime('%Y-%m-%d')

    # 转为日期格式
    datestart = datetime.strptime(datestart, '%Y-%m-%d')
    dateend = datetime.strptime(dateend, '%Y-%m-%d')
    date_list = [datestart.strftime('%Y-%m-%d')]
    while datestart < dateend:
        # 日期叠加一天
        datestart += timedelta(days=+1)
        # 日期转字符串存入列表
        date_list.append(datestart.strftime('%Y-%m-%d'))
    return date_list


def create_datetime_list(datetime_start: str = None, datetime_end: str = None,
                         time_delta: timedelta = timedelta(hours=6)):
    """ 根据开始日期时间、结束日期时间和时间间隔计算日期时间列表

    Args:
        datetime_start: 开始日期时间字符串， 格式如："2018-01-01 00:00"
        datetime_end: 结束日期时间字符串， 格式如："2018-01-01 00:00"
        time_delta: datetime.timedelta 对象

    Returns:
        固定时间间隔的日期时间列表 ['2020-01-25 00:00', '2020-01-25 06:00', '2020-01-25 12:00',...]
    """
    dt_start = datetime.strptime(datetime_start, "%Y-%m-%d %H:%M")
    dt_end = datetime.strptime(datetime_end, "%Y-%m-%d %H:%M")

    datetime_list = [dt_start.strftime('%Y-%m-%d %H:%M')]
    while dt_start < dt_end:
        # 日期叠加 time_delta
        dt_start += time_delta
        # 日期转字符串存入列表
        datetime_list.append(dt_start.strftime('%Y-%m-%d %H:%M'))

    return datetime_list


def jd2ce(JD):
    """将儒略日转换成公历日期（格里历）

    Args:
        JD: 儒略日

    Returns:
        公历的 Y 年 M 月 D 日

    """
    JD = JD + 0.5  # 以BC4713年1月1日0时为历元
    Z = math.floor(JD)
    F = JD - Z  # 日的小数部分
    if Z < 2299161:  # 儒略历
        A = Z
    else:  # 格里历
        a = math.floor((Z - 2305447.5) / 36524.25)
        A = Z + 10 + a - math.floor(a / 4)
    k = 0
    while True:
        B = A + 1524  # 以BC4717年3月1日0时为历元
        C = math.floor((B - 122.1) / 365.25)  # 积年
        D = math.floor(365.25 * C)  # 积年的日数
        E = math.floor((B - D) / 30.6)  # B-D为年内积日，E即月数
        day = B - D - math.floor(30.6 * E) + F
        if day >= 1: break  # 否则即在上一月，可前置一日重新计算
        A -= 1
        k += 1
    month = E - 1 if E < 14 else E - 13
    year = C - 4716 if month > 2 else C - 4715
    day += k
    if int(day) == 0:
        day += 1

    return year, month, day


def JD2AD(JD: float):
    """将儒略日转换成公历日期（格里历）， 有点问题！！！！！

    :param JD: 儒略日
    :returns: 公历的 Y 年 M 月 D 日

    参考鲁鹏的CSDN博客：[天文观测时间系统](https://blog.csdn.net/qq_25777815/article/details/104632275)
    """

    J = math.floor(JD + 0.5)
    N = math.floor(4 * (J + 68569) / 146097)
    L1 = J + 68569 - math.floor((N * 146097 + 3) / 4)
    Y1 = math.floor(4000 * (L1 + 1) / 1461001)
    L2 = L1 - math.floor(1461 * Y1 / 4) + 31
    M1 = math.floor(80 * L2 / 2447)
    D = L2 - math.floor(2447 * M1 / 80)
    L3 = math.floor(M1 / 11)
    M = M1 + 2 - 12 * L3
    Y = math.floor(100 * (N - 49) + Y1 + L3)
    return Y, M, D


def ce2jd(Year, Month, D):
    """将公历日期（格里历）转换成儒略日

    Args:
        Year: 公历年
        Month: 公历月份
        D: 公历日，包含小数部分

    Returns:
        儒略日
    """
    if Month in [1, 2]:
        M = Month + 12
        Y = Year - 1
    else:
        Y = Year
        M = Month
    B = 0
    if Y > 1582 or (Y == 1582 and M > 10) or (Y == 1582 and M == 10 and D >= 15):
        B = 2 - int(Y / 100) + int(Y / 400)  # 公元1582年10月15日以后每400年减少3闰
    JD = math.floor(365.25 * (Y + 4716)) + int(30.6 * (M + 1)) + D + B - 1524.5

    # if Y > 1858 or (Y == 1858 and M > 11) or (Y == 1858 and M == 11 and D >= 17):
    #     MJD = int(JD - 2400000.5)
    #     print("简化儒略日为：{}".format(MJD))
    return JD


def AD2JD(Y: int, M: int, D: float) -> float:
    """将公历日期（格里历）转换成儒略日

    :param Y: 公历年
    :param M: 公历月份
    :param D: 公历日
    :returns: 儒略日

    参考参考鲁鹏的CSDN博客：[天文观测时间系统](https://blog.csdn.net/qq_25777815/article/details/104632275)
    """

    # JD = D - 32075 + math.floor( 1461*(Y+4800+math.floor((M-14)/12))/4 ) + \
    #     math.floor(367*(M-2-math.floor((M-14)/12)*12)/12) - \
    #     math.floor(3*(Y+4900+math.floor((M-14)/12)/100)/4) - 0.5

    a = math.floor((14 - M) / 12)
    y = Y + 4800 - a
    m = M + 12 * a - 3

    JD = D + math.floor((153 * m + 2) / 5) + 365 * y + math.floor(y / 4) - \
         math.floor(y / 100) + math.floor(y / 400) - 32045 - 0.5

    return JD


def datetime2hours(date_time: datetime):
    """根据输入的公历日期时间(python datetime 类型) 计算改日期距离 1900-01-01 零点的小时数

    Args:
        date_time: 公历日期时间(python datetime 类型)

    Returns:
        从 1900-01-01 零点开始的小时数

    **注意**： 计算 hours 时日期的微秒数没有考虑， 精度到秒
    """
    date_time0 = datetime(1900, 1, 1, 0)
    time_delta = date_time - date_time0
    hours = time_delta.days*24 + time_delta.seconds/3600

    return hours


def hours2datetime(H: float):
    """将自 1900-01-01 零点的小时数转化为公历日期时间

    Args:
        H: 从 1900-01-01 零点开始的小时数

    Returns:
        公历日期时间 (python datetime 类型)

    """
    date_time0 = datetime(1900, 1, 1, 0)
    time_delta = timedelta(hours=H)
    date_time = date_time0 + time_delta

    return date_time
