from selenium import webdriver
import tkinter as tk
import time
import xlrd
import xlwt

login_window = tk.Tk()
login_window.title('统一身份认证登录——京工飞鸿')
user_name = tk.StringVar()
pass_word = tk.StringVar()


def change_state():
    username = user_name.get()
    password = pass_word.get()

    # 打开浏览器登录账号
    browser = webdriver.Firefox()
    browser.get("https://login.bit.edu.cn/authserver/login?service=http://campus.info.bit.edu.cn/shiro-cas")
    browser.find_element_by_id("username").send_keys(username)  # 填入账号
    browser.find_element_by_id("password").send_keys(password)  # 填入密码
    browser.find_element_by_class_name("login-btn").click()

    # 获取系统中所有受理申请的条目（一个人可能有几个受理申请状态的记录）
    browser.get("http://campus.info.bit.edu.cn/retrieveAgencyRequest/list")

    workbook_1 = xlrd.open_workbook('要改状态申请者的学号工号.xlsx')
    sheet_1 = workbook_1.sheets()[0]
    student_ids = sheet_1.col_values(0)
    workbook_2 = xlwt.Workbook()
    sheet_2 = workbook_2.add_sheet("受理申请")
    insert_row = 0
    for student_id in student_ids:
        student_id = str(student_id).split('.')[0]  # 避免学号带小数（“2020195062.0”）时报错
        routine = browser.find_element_by_id("routine")
        status = browser.find_element_by_id("status")
        webdriver.support.select.Select(routine).select_by_visible_text("学生公费医疗报销")
        webdriver.support.select.Select(status).select_by_visible_text("受理申请")
        browser.find_element_by_class_name("input.search-query").send_keys(student_id)
        browser.find_element_by_class_name("btn.btn-purple.bigger.btn-mini").click()
        table = browser.find_elements_by_tag_name("tr")
        row_num = len(table)
        if row_num == 1:  # 如果该申请者没有申请，输出”未申请“
            sheet_2.write(insert_row, 0, label="该申请者没有状态为受理申请的记录！")
            insert_row += 1
        elif row_num > 1:  # 打印所有申请记录
            for row in range(1, row_num):
                write_row(sheet_2, table[row].text, insert_row, 0)
                insert_row += 1
        else:
            insert_row += 1

    workbook_2.save("查询到受理申请的记录.xls")
    browser.close()


def write_row(sheet: xlwt.Workbook.add_sheet, string: str, row_num: int, col_start: int):
    info_list = string.split(' ')
    for num in range(len(info_list)):
        sheet.write(row_num, col_start + num, info_list[num])


login_window.geometry('310x100')

login_window.resizable(width=False, height=False)

l1 = tk.Label(login_window, text='用户名：').place(x=30, y=30)
l2 = tk.Label(login_window, text='密  码：').place(x=30, y=60)

w1 = tk.Entry(login_window, textvariable=user_name, width=20).place(x=80, y=30)
w2 = tk.Entry(login_window, textvariable=pass_word, width=20, show='*').place(x=80, y=60)
btn = tk.Button(login_window, text='改状态', command=change_state).place(x=240, y=40)

login_window.mainloop()
