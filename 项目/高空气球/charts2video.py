import os

from PIL import Image
import cv2


def charts2video(img_path, video_path):
    """将给定目录下的图片转成视频

    Args:
        img_path: 图片路径
        video_path: 输出视频的路径

    Returns: 图片转成的视频
    来源： https://www.cnblogs.com/willwuss/p/12695963.html
    """
    images = os.listdir(img_path)
    images.sort(key=lambda x: int(x[:-4]))  # 以名称字符串的第 1 到 7 位的数字从小到大排序　　
    fps = 2  # 帧数
    fourcc = cv2.VideoWriter_fourcc(*"MJPG")

    im = Image.open(img_path + images[0])
    video_writer = cv2.VideoWriter(video_path, fourcc, fps, im.size)
    for img_i in images:
        frame = cv2.imread(img_path + img_i)
        print('开始将 ' + img_i + ' 加入视频\n')
        video_writer.write(frame)
    video_writer.release()


if __name__ == '__main__':
    imgPath = "./windFieldCharts40km/"  # 图像路径
    videoPath = "./windCharts40km2018.avi"  # 视频路径和视频名称
    charts2video(imgPath, videoPath)
