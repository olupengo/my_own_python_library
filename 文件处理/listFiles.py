# function to list files in each folder of the current working directory

import os

def listFiles(startPath):

    for root, dirs, files in os.walk(startPath):
        # print(dirs)
        isRootContainGit = root.replace(startPath, '').count('.git')
        if not isRootContainGit:
            level = root.replace(startPath, '').count(os.sep)
            indent = ' ' * 4 * level
            print('{}{}/'.format(indent, os.path.basename(root)))
            subindent = ' ' * 4 * (level + 1)
            for f in files:
                print('{}{}'.format(subindent, f))