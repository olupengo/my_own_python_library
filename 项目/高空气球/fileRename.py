import os


path = './climateInfoNorthernHemisphere/'
file_name_list = os.listdir(path)
# print(len(file_name_list))

count = 0
for file in file_name_list:
    old_name = file
    new_name = file[-13:]

    count += 1
    os.renames(path+old_name, path+new_name)
    print(str(count) + file + ' 重命名成功\n')

print('重命名完成!\a\n')