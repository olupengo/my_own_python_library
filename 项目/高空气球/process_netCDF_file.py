import math
import numpy as np
from scipy.interpolate import interpn, interp1d
from netCDF4 import Dataset
import dateProcess  # 自己写的处理日期的函数
# import logging   # 用于生成日志
# logging.basicConfig(level=logging.DEBUG,  # 控制台打印的日志级别
#                     filename='get_wind_temp_pres.log',
#                     filemode='w',   # 有w和a，w就是写模式，每次都会重新写日志； a是追加
#                     format='%(asctime)s   %(levelname)s   %(message)s')

# dataset = netCDF4.Dataset('./climateInfo/2018010100.nc')
# print(dataset)
# print('-'*80 + '\n')

# 查看nc文件中的变量
# print(dataset.variables.keys())
# for var in dataset.variables.keys():
#     print(var)
# print('-'*80 + '\n')

# 查看每个变量的信息
# print(dataset.variables['longitude'])
# print(dataset.variables['latitude'])
# print(dataset.variables['level'])
# print(dataset.variables['time'])
# print(dataset.variables['t'])
# print(dataset.variables['u'])
# print(dataset.variables['v'])
# print('-'*80 + '\n')

# 查看每个变量的属性
# print(dataset.variables['t'].units)
# print(dataset.variables['t'].standard_name)
# print(dataset.variables['t'].scale_factor)
# print(dataset.variables['t'].add_offset)

# 读取数据
# data = dataset.variables['t'][0][0][0]
# print(data)

# 列出所有 dimensions
# for d in dataset.dimensions.items():
#     print(d)

#  extract lon/lat/level/time values to numpy arrays
# lon, lat = dataset.variables['longitude'][:], dataset.variables['latitude'][:]
# level = dataset.variables['level'][:]
# time = dataset.variables['time'][:]
# print(lon)
# print(lat)
# print(level)
# print(time)
# temp = dataset.variables['t'][:]  # 读取数据库中的温度
# v_com = dataset.variables['v'][:]  # 读取数据库中风速的北向分量（南风为正）
# u_com = dataset.variables['u'][:]  # 读取数据库中风速的东向分量（西风为正）
# p_level = dataset.variables['level'][:]  # 读取数据库中的压力等级


def compute_scale_and_offset(min_value, max_value, num_bit):
    """
    采集到原始数据后，为了节省存储空间，保证存储数据的精度引入 scale_factor 和 add_offset
    
    Args:
        min_value: 整个数据集的最小值
        max_value: 整个数据集的最大值
        num_bit: 转化为多少个二进制数的整数，取决于你数据的范围，我这个数据集是16进制度

    Returns:
        返回 scale_factor 和 add_offset

    Examples:
        >>> # 我调用上述函数计算了偏移和缩放值：
        >>> import numpy as np
        >>> print(compute_scale_and_offset(np.array(dataset['t'][:]).min(),np.array(dataset['t'][:]).max(),16))
        >>> # 得到的结果和我读取的ERA5文件中的scale_factor, add_offset一致。

    Notes: 使用 netCDF 模块读取的数据无须再进行处理，该模块读取的是转换后的实际数据
    原文链接：https://blog.csdn.net/Will_Zhan/article/details/105388609
    """

    # stretch/compress data to the available packed range
    scale_factor = (max_value - min_value) / (2 ** num_bit - 1)
    # translate the range to be symmetric about zero
    add_offset = min_value + 2 ** (num_bit - 1) * scale_factor
    return scale_factor, add_offset


def get_wind_temp_pres(time: float, alt: float, lat: float, lon: float, R_specific=287.058):
    """
    通过输入时间 time、高度 alt、纬度 lat、经度 lon 输出风速(m/s)、温度(k)、压强(hPa) 和密度(kg/m^3)

    Args:
        time: 距离公历 1900-01-01 00:00:00.0 的小时数, 1034376 <= time <= 1043130
        alt: 海拔高度 (m), 149.265 <= alt <= 47289.4
        lat: 纬度 (deg), 18 <= lat <= 51
        lon: 经度 (deg), 111 <= lon <= 243
        R_specific: specific gas constant for dry air is 287.058 (J/(kg·K))

    Returns: 给定地点的风速温度 (u, v)、温度 temperature、压力 pressure 和密度 rho
        u: 东向风速(m/s)，西风为正, 东风为负
        v: 北向风速(m/s)，南风为正，北风为负
        temperature: 大气温度(K)，开式温度减去 273.15 得到摄氏温度
        pressure: 大气压力(hPa), 1hPa = 100Pa
    """

    jd0 = dateProcess.ce2jd(1900, 1, 1)

    # 寻找时间区间 [time_left, time_right], 使得 time_left <= time <=time_right
    time_left = (time // 6) * 6  # 获取比 time 小且最接近气候数据表中时间的时间点
    time_right = time_left + 6  # 比 time 大且最接近气候数据表中时间的时间点 (hour)
    jd_left = jd0 + time_left / 24  # time_left 对应的儒略日
    jd_right = jd0 + time_right / 24  # time_right 对应的儒略日
    ad_left = dateProcess.jd2ce(jd_left)  # time_left 对应的公历日期
    ad_right = dateProcess.jd2ce(jd_right)  # time_right 对应的公历日期
    # logging.info("时间区间端点的公历日期： {}，{}".format(ad_left, ad_right))

    day_left = math.floor(ad_left[2])  # 提取天的整数部分
    day_right = math.floor(ad_right[2])
    hour_left = (ad_left[2] - math.floor(ad_left[2])) * 24  # 将天的小数部分转化成小时
    hour_right = (ad_right[2] - math.floor(ad_right[2])) * 24
    # logging.info("时间区间端点的小时： {}，{}".format(hour_left, hour_right))

    # 读取时间区间端点数据
    dataset1 = Dataset('./climateInfo/' + str(ad_left[0])
                       + str(ad_left[1]).zfill(2) + str(day_left).zfill(2)
                       + str(int(np.round(hour_left))).zfill(2) + '.nc')
    dataset2 = Dataset('./climateInfo/' + str(ad_left[0])
                       + str(ad_right[1]).zfill(2) + str(day_right).zfill(2)
                       + str(int(np.round(hour_right))).zfill(2) + '.nc')
    # print(dataset1, dataset2)

    lon1, lat1 = dataset1.variables['longitude'][:], dataset1.variables['latitude'][:]  # 读取经纬度
    level1 = dataset1.variables['level'][:]  # 读取压力水平

    temp1 = dataset1.variables['t'][:]  # 读取数据库中的温度
    v_com1 = dataset1.variables['v'][:]  # 读取数据库中风速的北向分量（南风为正）
    u_com1 = dataset1.variables['u'][:]  # 读取数据库中风速的东向分量（西风为正）

    temp2 = dataset2.variables['t'][:]  # 读取数据库中的温度
    v_com2 = dataset2.variables['v'][:]  # 读取数据库中风速的北向分量（南风为正）
    u_com2 = dataset2.variables['u'][:]  # 读取数据库中风速的东向分量（西风为正）

    altitude = np.array([47289.4, 42144.8, 39213.3, 35578.9, 33217.3, 30767.3, 26203.2, 23622.5,
                         20474.1, 18481.4, 16421.3, 15103.5, 13989.6, 13016.6, 12158, 11395.1,
                         10709.5, 9491.32, 8419.41, 7459.67, 6589.09, 5791.35, 5056.06, 4374.31,
                         3737.93, 3139.48, 2572.26, 2299.07, 2033.31, 1774.26, 1522.65, 1276.99,
                         1038.78, 808.772, 583.973, 365.133, 149.265])
    # 线性插值
    pressure = interp1d(altitude, level1, kind='linear')(np.array([alt]))
    point = np.array([pressure[0], -lat, lon])  # 插值的点
    points = (level1, -lat1, lon1)  # 自变量

    u1 = interpn(points, u_com1[0], point)  # 对 dataset1 插值
    v1 = interpn(points, v_com1[0], point)
    temperature1 = interpn(points, temp1[0], point)

    u2 = interpn(points, u_com2[0], point)  # 对 dataset2 插值
    v2 = interpn(points, v_com2[0], point)
    temperature2 = interpn(points, temp2[0], point)
    # logging.info("dataset1 和 dataset2 中 u， v， temperature 的插值结果: {},{},{} ; {},{},{}".format(
    #     u1[0], v1[0], temperature1[0], u2[0], v2[0], temperature2[0]
    # ))

    u = interp1d(np.array([time_left, time_right]), np.array([u1[0], u2[0]]))(np.array([time]))
    v = interp1d(np.array([time_left, time_right]), np.array([v1[0], v2[0]]))(np.array([time]))
    temperature = interp1d(np.array([time_left, time_right]), np.array([temperature1[0],
                                                                        temperature2[0]]))(np.array([time]))

    # 计算空气密度，公式来自维基百科：[Density of air](https://en.wikipedia.org/wiki/Density_of_air)
    rho = pressure[0]*100 / (R_specific*temperature[0])
    return np.array([u[0], v[0], temperature[0], pressure[0], rho])


if __name__ == '__main__':
    # 对函数 get_wind_temp_pres() 进行测试
    for i in range(0, 10000):
        time_min, time_max = 1034376, 1043130
        alt_min, alt_max = 149.265, 47289.4
        lat_min, lat_max = 18, 51
        lon_min, lon_max = 111, 243
    
        time = np.random.random()*(time_max - time_min) + time_min
        alt = np.random.random()*(alt_max - alt_min) + alt_min
        lat = np.random.random()*(lat_max - lat_min) + lat_min
        lon = np.random.random()*(lon_max - lon_min) + lon_min
    
        print("-"*80 + "\n")
        print("第 %d 次测试结果为： " % i)
        print(get_wind_temp_pres(time, alt, lat, lon))
        print("\n")

    # # 验证 MATLAB 调用时传入向量的情况，有问题！！！！
    # get_wind_temp_pres(array.array('d', [1034376, 1043130]),    # time
    #                    array.array('d', [149.265, 149.265]),    # altitude
    #                    array.array('d', [18, 18]),              # latitude
    #                    array.array('d', [111, 111]) )          # longitude
