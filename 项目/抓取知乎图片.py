import re
from selenium import webdriver
import time
import urllib.request

driver = webdriver.Firefox()    # 打开浏览器
driver.maximize_window()        # 最大化浏览器
# driver.get("https://www.zhihu.com/question/29134042")
# driver.get("https://zhuanlan.zhihu.com/p/63763400")
driver.get("https://lupeng.fun/cong-c-yuan-wen-jian-dao-ke-zhi-xing-wen-jian/")    # 打开网址，获取网页信息

i = 0

while i < 10:

    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(2)

    try:
        driver.find_element_by_css_selector('button.QuestionMainAction').click()
        print("page" + str(i))
        time.sleep(1)
    except:
        break

result_raw = driver.page_source
print(result_raw)
content_list = re.findall("img src=\"(.+?)\" ", str(result_raw))

if len(content_list) == 0:
    print("No picture found.")
    exit(1)

n = 0

while n < len(content_list):
    
    try:
        i = time.time()
        local = (r"%s.jpg" % (i))
        urllib.request.urlretrieve(content_list[n], local)
        print("编号：" + str(i))
        n = n + 1
    except:
        n = n + 1
        continue
    
time.sleep(2)
driver.close()   # close the browser