#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 31 20:11:39 2020

@author: peng
"""
from PIL import Image

im = Image.open("1.jpg")
images = []
images.append(Image.open('2.jpg'))
images.append(Image.open('3.jpg'))

im.save('gif.gif', save_all=True, append_images=images, loop=1, duration=1,
        comment=b"aaabb")