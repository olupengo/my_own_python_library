from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import tkinter as tk
import xlrd

login_window = tk.Tk()
login_window.title('统一身份认证登录——京工飞鸿')
user_name = tk.StringVar()
pass_word = tk.StringVar()


def change_state():
    username = user_name.get()
    password = pass_word.get()

    # 打开浏览器登录账号
    browser = webdriver.Firefox()
    browser.get("https://login.bit.edu.cn/authserver/login?service=http://campus.info.bit.edu.cn/shiro-cas")
    browser.find_element_by_id("username").send_keys(username)  # 填入账号
    browser.find_element_by_id("password").send_keys(password)  # 填入密码
    browser.find_element_by_class_name("login-btn").click()

    # 获取系统中所有受理申请的条目（一个人可能有几个受理申请状态的记录）
    browser.get("http://campus.info.bit.edu.cn/retrieveAgencyRequest/list")

    workbook_1 = xlrd.open_workbook('./查询到受理申请的记录.xls')  # 已经将不对的条目踢除
    sheet_1 = workbook_1.sheets()[0]
    codes = sheet_1.col_values(0)  # 要更改状态的申请单编码
    names = sheet_1.col_values(2)  # 名单

    for i, code_i in enumerate(codes):
        code_i = str(code_i).split('.')[0]  # 避免带code_i小数（“2020195062.0”）时报错
        # 搜索申请单编码
        browser.find_element_by_class_name("input.search-query").send_keys(code_i)
        browser.find_element_by_class_name("btn.btn-purple.bigger.btn-mini").click()
        browser.find_element_by_class_name("btn.btn-mini.btn-info").click()
        # 选择办理完成单选框，并确认
        radios = browser.find_elements_by_xpath("//*/input[@type='radio']")  # 获取所有单选框
        radios[4].click()  # 选择第五个选项——”办理完成“
        browser.find_element_by_id("confirm-button").click()
        # 再次确认更改状态
        button = browser.find_element_by_xpath('//a[@class="btn btn-primary"]')
        while not button.is_displayed():  # 等待确认按钮出现，再点击
            time.sleep(0.01)
        button.click()

        print(f'{code_i} {names[i]} 状态更改完成！')

    browser.close()


login_window.geometry('310x100')

login_window.resizable(width=False, height=False)

l1 = tk.Label(login_window, text='用户名：').place(x=30, y=30)
l2 = tk.Label(login_window, text='密  码：').place(x=30, y=60)

w1 = tk.Entry(login_window, textvariable=user_name, width=20).place(x=80, y=30)
w2 = tk.Entry(login_window, textvariable=pass_word, width=20, show='*').place(x=80, y=60)
btn = tk.Button(login_window, text='改状态', command=change_state).place(x=240, y=40)

login_window.mainloop()
