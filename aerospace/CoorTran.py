''' 
坐标变换(coordinate transformations)矩阵和旋转矩阵(Rotation matrix)，适用的坐标系为笛卡尔右手系
角度使用的单位为弧度(radian)
坐标变换：将一个坐标系中的坐标转换到另一个坐标系
旋转：求某一向量旋转后的坐标，前后两个坐标表示在一个坐标系
author:Peng Lu

'''
import numpy as np


class CoorTran():
    '''CoorTran类包含了常用的坐标转换矩阵，适用的坐标系为笛卡尔右手系'''
    
    def cx(self, alpha):
        '''绕x轴旋转的alpha弧度的坐标转换矩阵'''
        conMatCx = np.array([[1,0,0],
                             [0,np.cos(alpha),np.sin(alpha)],
                             [0,-np.sin(alpha),np.cos(alpha)]])
        return conMatCx
    
    def cy(self, alpha):
        '''绕y轴旋转的alpha弧度的坐标转换矩阵'''
        conMatCy = np.array([[np.cos(alpha),0,-np.sin(alpha)],
                             [0,1,0],
                             [np.sin(alpha),0,np.cos(alpha)]])
        return conMatCy
    
    def cz(self, alpha):
        '''绕z轴旋转的alpha弧度的坐标转换矩阵'''
        conMatCz = np.array([[np.cos(alpha),np.sin(alpha),0],
                             [-np.sin(alpha),np.cos(alpha),0],
                             [0,0,1]])
        return conMatCz
        
    def cvb(self, alpha, beta):
        '''将速度坐标系先绕y轴旋转beta角，然后绕z轴旋转alpha角，可与本体系重合
        使用的旋转类型：intrinsic rotations
        reference book: 钱杏芳等. 导弹飞行力学[M]. 2000.
        
        Parameters
        -------
        alpha : 攻角
        beta : 侧滑角
        
        Examples
        -------
        
        '''
        conMatCvb = np.array([[np.cos(alpha)*np.cos(beta), np.sin(alpha), -np.cos(alpha)*np.sin(beta)],
                              [-np.cos(beta)*np.sin(alpha), np.cos(alpha), np.sin(alpha)*np.sin(beta)],
                              [np.sin(beta), 0, np.cos(beta)]])
        return conMatCvb

    def cnb(self, theta, psi, gamma):
        '''地面坐标系先绕y轴旋转psi角，再绕z轴旋转theta角，最后绕x轴旋转gamma角，可与本体系重合
        使用的旋转类型：intrinsic rotations
        reference book: 钱杏芳等. 导弹飞行力学[M]. 2000.
        
        Parameters
        -----
        theta : 俯仰角
        psi : 偏航角
        gamma : 倾斜角(又叫滚转角)
        
        Examples
        -----
        
        '''
        conMatCnb = np.array([[np.cos(theta)*np.cos(psi), np.sin(theta), -np.cos(theta)*np.sin(psi)],
                              [-np.sin(theta)*np.cos(psi)*np.cos(gamma)+np.sin(psi)*np.sin(gamma),
                                np.cos(theta)*np.cos(gamma), 
                                np.sin(theta)*np.sin(psi)*np.cos(gamma)+np.cos(psi)*np.sin(gamma)],
                              [np.sin(theta)*np.cos(psi)*np.sin(gamma)+np.sin(psi)*np.cos(gamma), 
                                -np.cos(theta)*np.sin(gamma), 
                                -np.sin(theta)*np.sin(psi)*np.sin(gamma)+np.cos(psi)*np.cos(gamma)]])
        return conMatCnb

    def cnb1(self, theta, psi, gamma):
        '''地面坐标系先绕z轴旋转theta角，再绕y轴旋转psi角，最后绕x轴旋转gamma角，可与本体系重合
        和Cnb旋转顺序不同。theta also refered to as yaw, psi refered to as pitch, gamma as roll
        使用的旋转类型：extrinsic rotations
        
        Parameters
        ------
        theta : 俯仰角
        psi : 偏航角
        gamma : 倾斜角(又叫滚转角)
        此处的俯仰角、偏航角、倾斜角和常规的俯仰角、偏航角、倾斜角定义不同，需注意！
        
        Examples
        -------
        '''
        conMatCnb = np.array([[np.cos(theta)*np.cos(psi), np.sin(theta)*np.cos(psi), -np.sin(psi)],
                              [np.cos(theta)*np.sin(psi)*np.sin(gamma)-np.sin(theta)*np.cos(gamma),
                                np.sin(theta)*np.sin(psi)*np.sin(gamma)+np.cos(theta)*np.cos(gamma),
                                np.cos(psi)*np.sin(gamma)],
                              [np.cos(theta)*np.sin(psi)*np.cos(gamma)+np.sin(theta)*np.sin(gamma),
                                np.sin(theta)*np.sin(psi)*np.cos(gamma)-np.cos(theta)*np.sin(gamma),
                                np.cos(psi)*np.cos(gamma)]])
        return conMatCnb

    # 地面坐标系中的某个坐标，分别与坐标转换矩阵cnb和cnb1相乘，得到的坐标是否相同？通过case2可知得到的坐标不同！
    # case1: theta = 90deg, psi = gamma = 0
    # print(np.dot(cnb(np.pi/2,0,0), np.array([[1],[0],[0]])))
    # print(np.dot(cnb1(np.pi/2,0,0), np.array([[1],[0],[0]])))
    # case2: theta = psi = gamma = np.pi/6
    # print(np.dot(cnb(np.pi/6,np.pi/6,np.pi/6), np.array([[1],[0],[0]])))
    # print(np.dot(cnb1(np.pi/6,np.pi/6,np.pi/6), np.array([[1],[0],[0]])))


class RotMat():
    '''RotMat类包含了常用的旋转矩阵，适用的坐标系为笛卡尔右手系'''
    def rx(self, alpha):
        '''绕x轴旋转的alpha弧度的旋转矩阵'''
        rotMatrix = np.array([[1,0,0],
                             [0,np.cos(alpha),-np.sin(alpha)],
                             [0,np.sin(alpha),np.cos(alpha)]])
        return rotMatrix

    def ry(self, alpha):
        '''绕y轴旋转的alpha弧度的旋转矩阵'''
        rotMatrix = np.array([[np.cos(alpha),0,np.sin(alpha)],
                             [0,1,0],
                             [-np.sin(alpha),0,np.cos(alpha)]])
        return rotMatrix

    def rz(self, alpha):
        '''绕z轴旋转的alpha弧度的旋转矩阵'''
        rotMatrix = np.array([[np.cos(alpha),-np.sin(alpha),0],
                             [np.sin(alpha),np.cos(alpha),0],
                             [0,0,1]])
        return rotMatrix
    
    def R(self, theta, psi, phi):
        '''NASA Standard Aeroplane
        order:z(φ),y(θ),x(ψ)，每次都是绕同一个坐标系的轴旋转，使用的旋转类型：extrinsic rotations
        reference website: http://www.euclideanspace.com/maths/geometry/rotations/euler/indexLocal.htm

        Parameters
        -----
        theta : y(θ)
        psi : x(ψ)
        phi : z(φ)
        '''
        rotMatrix = np.array([[np.cos(theta)*np.cos(phi),-np.cos(theta)*np.sin(phi),np.sin(theta)],
                              [np.cos(psi)*np.sin(phi)+np.sin(psi)*np.sin(theta)*np.cos(phi),
                               np.cos(psi)*np.cos(phi)-np.sin(psi)*np.sin(theta)*np.sin(phi),
                               -np.sin(psi)*np.cos(theta)],
                              [np.sin(psi)*np.sin(phi)-np.cos(psi)*np.sin(theta)*np.cos(phi),
                               np.sin(psi)*np.cos(phi)+np.cos(psi)*np.sin(theta)*np.sin(phi),
                               np.cos(psi)*np.cos(theta)]])
        return rotMatrix
    
    def R1(self, theta, psi, phi):
        '''NASA Standard Aeroplane (reversed order)
        order:x(ψ),y(θ),z(φ)，每次都是绕同一个坐标系的轴旋转，使用的旋转类型：extrinsic rotations
        
        Parameters
        -----
        theta : y(θ)
        psi : x(ψ)
        phi : z(φ)
        '''
        rotMatrix = np.array([[np.cos(psi)*np.cos(theta),
                               np.sin(psi)*np.cos(phi)+np.cos(psi)*np.sin(theta)*np.sin(phi),
                               np.sin(psi)*np.sin(phi)-np.cos(psi)*np.sin(theta)*np.cos(phi)],
                              [-np.sin(psi)*np.cos(theta),
                               np.cos(psi)*np.cos(phi)-np.sin(psi)*np.sin(theta)*np.sin(phi),
                               np.cos(psi)*np.sin(phi)+np.sin(psi)*np.sin(theta)*np.cos(phi)],
                              [np.sin(theta),-np.cos(theta)*np.sin(phi),np.cos(theta)*np.cos(phi)]])
        return rotMatrix
    
    def R2(self, theta, psi, phi):
        '''NASA Standard Aerospace
        order:z(φ),y(θ),z(ψ)，使用的旋转类型：extrinsic rotations
        
        Parameters
        -----
        theta : 
        psi : 
        phi : 
        '''
        rotMatrix = np.array([[np.cos(psi)*np.cos(theta)*np.cos(phi)-np.sin(psi)*np.sin(phi),
                               -np.cos(psi)*np.cos(theta)*np.sin(phi)-np.sin(psi)*np.cos(phi),
                               np.cos(psi)*np.sin(theta)],
                              [np.sin(psi)*np.cos(theta)*np.cos(phi)+np.cos(psi)*np.sin(phi),
                               -np.sin(psi)*np.cos(theta)*np.sin(phi)+np.cos(psi)*np.cos(phi),
                               np.sin(psi)*np.sin(theta)],
                              [-np.sin(theta)*np.cos(phi),np.sin(theta)*np.sin(phi),np.cos(theta)]])
        return rotMatrix

#gamma = np.pi/10
#theta = np.pi/6
#psi = np.pi/4
#C = CoorTran() #instantiate
#R = RotMat()   #instantiate
#RotationMatrix = np.dot(np.dot(R.rx(gamma),R.rz(theta)),R.ry(psi))  #order:y,z,x
#x_after_rot = np.dot(RotationMatrix,np.array([[1],[0],[0]]))
#y_after_rot = np.dot(RotationMatrix,np.array([[0],[1],[0]]))
#z_after_rot = np.dot(RotationMatrix,np.array([[0],[0],[1]]))
#np.dot(C.cnb(theta,psi,gamma).transpose(),np.array([[0],[0],[1]]))