"""

绘制一年的风场

"""

import matplotlib.pyplot as plt
import numpy as np
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import dateProcess
from cartopy.feature.nightshade import Nightshade
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
from datetime import datetime
from process_netCDF_file import get_wind_temp_pres  # 根据数据集插值的函数


def sample_wind_data(time, alt=12000):
    """
    获取给定时间和高度的风场数据
    Args:
        time: hours since 1900-01-01:00 (hour)
        alt: 高度 (m)， 默认值 12000m

    Returns:
        x: 经度 lon
        y: 纬度 lat
        u: 纬向风分量
        v: 经向风分量
        c: 箭头颜色数组，可为一种颜色，也可为颜色数组
    """
    num_x, num_y = 44, 11  # 经纬度划分的离散点个数
    x = np.linspace(111, 360 - 117, num_x)  # 离散经度
    y = np.linspace(18, 51, num_y)  # 离散纬度

    x_mesh, y_mesh = np.meshgrid(x, y)
    wind_info = np.zeros((num_y, num_x, 5))
    u, v = np.zeros((num_y, num_x)), np.zeros((num_y, num_x))
    for i in range(num_x):
        for j in range(num_y):
            wind_info[j][i] = get_wind_temp_pres(time, alt, y_mesh[j][i], x_mesh[j][i])
            u[j][i] = wind_info[j][i][0]
            v[j][i] = wind_info[j][i][1]

    return x, y, u, v


def wind_field_plot(arg_time, arg_alt):
    """
    根据高度和时间绘制风场
    Args:
        arg_time: 距离公历 1900-01-01 00:00:00.0 的小时数 (hour)
        arg_alt: 海拔高度 (m)

    Returns:
        风场图片
    """
    fig = plt.figure(figsize=(15, 5))
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree(central_longitude=180))

    box = [111, 360 - 117, 18, 51]  # [西边经度，东边经度，南边纬度，北边纬度]
    ax.set_extent(box, crs=ccrs.PlateCarree())

    scale = '50m'
    lon_step, lat_step = 22, 11
    land = cfeature.NaturalEarthFeature('physical', 'land', scale, edgecolor='face',
                                        facecolor=cfeature.COLORS['land'])
    ocean = cfeature.NaturalEarthFeature('physical', 'ocean', scale, edgecolor='face',
                                         facecolor=cfeature.COLORS['water'])

    ax.add_feature(land)
    ax.add_feature(ocean)
    # ax.add_feature(cfeature.BORDERS, linestyle='-.')
    # ax.add_feature(cfeature.OCEAN, zorder=0)
    # ax.add_feature(cfeature.LAND, zorder=0, edgecolor='black')

    date_time_arg_time = dateProcess.hours2datetime(arg_time)  # 将距离 1900-01-01 零点的小时数转化为公历日期
    ax.add_feature(Nightshade(date_time_arg_time, alpha=0.2))  # 夜色特征

    # 标注坐标轴
    ax.set_xticks(np.arange(box[0], box[1] + lon_step, lon_step), crs=ccrs.PlateCarree())
    ax.set_yticks(np.arange(box[2], box[3] + lat_step, lat_step), crs=ccrs.PlateCarree())
    # zero_direction_label用来设置经度的0度加不加E和W
    lon_formatter = LongitudeFormatter(zero_direction_label=False)
    lat_formatter = LatitudeFormatter()
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)

    x, y, u, v = sample_wind_data(arg_time, arg_alt)  # 从数据集读取经纬度 x,y 和风速 u,v
    ax.streamplot(x, y, u, v, transform=ccrs.PlateCarree())
    # ax.quiver(x, y, u, v, transform=ccrs.PlateCarree())
    plt.title(
        datetime.strftime(date_time_arg_time, "%Y-%m-%d %H:%M") + ' ' + str(arg_alt / 1000) + "km wind stream plot")

    ax.grid()
    # plt.show()
    plt.savefig('./windFieldCharts' + str(int(arg_alt/1000)) + 'km/' +
                str(arg_time).split('.')[0] + '.png', bbox_inches='tight')
    plt.close()


def main():
    start_datetime = "2018-01-01 00:00"
    end_datetime = "2018-12-31 18:00"

    # 获取 start_date 与 end_date 之间所有的日期列表
    datetime_list = dateProcess.create_datetime_list(start_datetime, end_datetime)

    altitude = [30000, 40000]  # 高度列表
    for alt in altitude:
        for datetime_i in datetime_list:
            hours_from_1990010100 = dateProcess.datetime2hours(datetime.strptime(datetime_i, '%Y-%m-%d %H:%M'))
            wind_field_plot(hours_from_1990010100, alt)
            print(datetime_i + ' 的风场图绘制完成\n')


if __name__ == '__main__':
    main()
